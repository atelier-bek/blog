<header>
  <ul id="coordonnees">
    <li>Atelier Bek</li>
    <ul>
      <li>Rue Du Ruisseau 15</li>
      <li>1080 Molenbeek-Saint-Jean</li>
      <li>Contact@atelier-bek.be</li>
    </ul>
  </ul>
  <ul id="nav">
    <li class="Evenements">Événements</li>
    <ul>
      <li>La Matrice<sup> - 09/12/2016</sup></li>
      <li class="girls">Girls in Molenbeek</li>
      <li class="bouturefest">Bouture Fest'</li>
      <li>Workshop Metapost</li>
      <li>Workshop textile</li>
      <li>La Perruque lancement #1</li>
    </ul>
    <li class="Chantiers">Chantiers</li>
    <ul>
      <li>La Matrice</li>
      <li>Identité visuelle Bek</li>
    </ul>
    <li>Outils</li>
    <ul>
      <li>Typothèque</li>
      <li>Fonderie Bek</li>
      <li>Gitlab</li>
    </ul>
  </ul>
  <ul class="Atelier">
    <li>L'atelier</li>
    <ul>
      <li class="blog">Blog</li>
      <li>À propos</li>
      <li>Contact</li>
    </ul>
    <ul class="Membres">
      <li>Membres</li>
      <ul>
        <li>Cecilia Borettaz</li>
        <li>Maria Delamarre</li>
        <li><a href="http://antoine-gelgon.fr/" target="_blank">Antoine Gelgon</a></li>
        <li>Fanny Devaux</li>
        <li>Émile Greis</li>
        <li><a href="http://cargocollective.com/lucaslejeune/" target='_blank'>Lucas Lejeune</a></li>
        <li>Alice Lapalu</li>
        <li><a href="http://alexandre.atelier-bek.be/" target="_blank">Alexandre Liziard</a></li>
        <li>Léonard Mabille</li>
        <li><a href="http://romainmarula.fr/" target="_blank">Romain Marula</a></li>
        <li><a href="http://matthieumichaut.tumblr.com/" target="_blank">Matthieu Michaut</a></li>
        <li><a href="http://etienneozeray.fr" target="_blank">Étienne Ozeray</a></li>
        <li><a href="http://baptiste-tosi.eu/" target="_blank">Baptiste Tosi</a></li>
        <li>Marie Trossat</li>
        <li><a href="https://morganetrouillet.wordpress.com/" target="_blank">Morgane Trouillet</a></li>
        <li>Stéphanie Vérin</li>
        <li><a href="http://www.xxx-clairewilliams-xxx.com/" target="_blank">Claire Williams</a></li>
      </ul>
    </ul>
  </ul>

</header>
