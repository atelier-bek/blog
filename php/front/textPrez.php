<div id="textPrez">
L’Atelier Bek est un environnement commun accueillant des pratiques pluridisciplinnaires. L’équipe actuelle se compose de designers, illustrateurs, artistes, plasticiens et artisans. En perpetuelle mutation, nous sommes convaincus que l’échange, le partage et la mutualisation des savoirs et expériences sauront modeler nos métiers avec effervescence et enthousiasme. <span>×</span>
</div>
