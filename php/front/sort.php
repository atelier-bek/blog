<?php

  $categories = array();
  $authors = array();
  $dates = array();

  for ($i = 0; $i < $count; $i++){

    if ($dirs[$i] == 'count') continue;

    $xml = simplexml_load_file('../datas/'.$dirs[$i].'/datas.xml');

    $categoryArr = explode(',', $xml->category);
    foreach ($categoryArr as $category){
      array_push($categories, $category);
    }

    $authorArr = explode(',', $xml->author);
    foreach ($authorArr as $author){
      array_push($authors, $author);
    }

    array_push($dates, $xml->date);

  }

  $categories = array_unique($categories);
  sort($categories);

  $authors = array_unique($authors);
  sort($authors);

  $dates = array_unique($dates);
  sort($dates);

?>


<div id="sort">

  <h1>FILTRER</h1>

  <ul class="categories colonne">
     <li class="title"><h2>Catégories</h2></li>
    <li id="catTous" class="checked"> tous</li>
    <?php foreach($categories as $category){
      $categorySlug = str_replace($a, $b, $category);
      ?>
      <li id="<?= $categorySlug ?>" class="checked"> <?= $category ?></li>
    <?php } ?>
  </ul>

  <ul class="personnes colonne">
    <li class="title"><h2>Personnes</h2></li>
     <li id="authorTous" class="checked"> tous</li>
     <?php foreach($authors as $author){
       $authorSlug = str_replace($a, $b, $author);
       ?>
       <li id="<?= $authorSlug ?>" class="checked"> <?= $author ?></li>
     <?php } ?>
   </ul>

  <ul class="date colonne">
    <li class="title"><h2>Date</h2></li>
     <li id="dateTous" class="checked"> tous</li>
    <?php foreach($dates as $date){ ?>
      <li id="<?= $date ?>" class="checked"> <?= $date ?></li>
    <?php } ?>
  </ul>

</div>
