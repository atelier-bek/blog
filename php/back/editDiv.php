    <?php include "../../inc/variables.php"; ?>
    <?php

    $id = $_POST["id"];

    echo "<div id=\"id\">$id</div>";

    $item = $datasDir . "/" . $id;

    # load datas.xml
    $xml = simplexml_load_file($item . "/datas.xml");

    # get datas
    $sendDateStr = $xml->date;
    $sendTimeStr = $xml->time;
    $textStr = $xml->text;


    ?>

    <div id="dateArea">
	<h2>Date</h2>
	<p>Format : année-mois-jour, laissez vide pour date actuelle.</p>
	<input id="date" type="text" value="<?php echo $sendDateStr; ?>"></input>
    </div>

    <div id="timeArea">
	<h2>Heure</h2>
	<p>Format : heures-minutes-secondes, laissez vide pour heure actuelle.</p>
	<input id="time" type="text" value="<?php echo $sendTimeStr; ?>"></input>
    </div>

    <div id="authorArea">
	<h2>Auteur(s)</h2>
	<p>Sélectionner les auteur(s).</p>
	<?php
	foreach ($authorArr as $author) {
	    echo "<input value=\"$author\" name=\"author\" type=\"checkbox\">$author</input>";
	}

	?>
    </div>

    <div id="categoryArea">
	<h2>Categorie(s)</h2>
	<p>Sélectionner les catégorie(s).</p>
	<?php
	foreach ($categoryArr as $category) {
	    echo "<input value=\"$category\" name=\"category\" type=\"checkbox\">$category</input>";
	}

	?>
    </div>

    <div id="textArea">
	<h2>Message</h2>
	<p>Entrer message ici. Pour ajouter des vidéos formattez comme ceci: <span style="color: grey">(video)[www.youtube.com/watch?v=LO40CaFEG5Y]</span>, pour des liens <span style="color: grey">(liens)[http://monlien.be]</span>.</p>
	<textarea id="text"><?php echo $textStr; ?></textarea>
    </div>

    <div id="fileArea">
	<h2>Fichier(s)</h2>
	<p>Sélectionner les fichiers à uploader.</p>
	<input id="file" name="file[]" multiple="multiple" type="file">
	<button id="fileCleanBtn">Clean file(s)</button>
    </div>

    <div id="sendArea">
	<h2>Envoyer</h2>
	<p>Verifier message et envoyer.</p>
	<button id="sendBtn">Envoyer</button>
    </div>
