<html>

    <head>

	<!-- inc -->
	<?php include "inc/meta.php"; ?>
	<?php include "inc/title.php"; ?>

	<!-- css -->
	<link rel="stylesheet" type="text/css" href="css/reset.css" />
	<link rel="stylesheet/less" type="text/css" href="css/columns.less" />
	<link rel="stylesheet/less" type="text/css" href="css/styles.less" />
	<link rel="stylesheet/less" type="text/css" href="css/back.less" />
	<link rel="stylesheet/less" type="text/css" href="css/datasDiv.less" />

	<!-- fonts -->
	<link rel="stylesheet" type="text/css" href="fonts/fonts.css" />

	<!-- lib -->
	<script src="lib/less.min.js" type="text/javascript"></script>
	<script src="lib/jquery-2.1.4.min.js" type="text/javascript"></script>

    </head>
    <body>


  	<?php include "inc/variables.php"; ?>

  	<div class="p-width-2" id="datasDiv">
  	</div>

    <?php

    // Define your username and password
    $username = "admin"; // à changer pour le serveur
    $password = "admin"; // à changer pour le serveur

    if ($_POST['txtUsername'] != $username || $_POST['txtPassword'] != $password){

      ?>

      <h1>Login</h1>

      <form name="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
          <p><label for="txtUsername">Username:</label>
          <br /><input type="text" title="Enter your Username" name="txtUsername" /></p>

          <p><label for="txtpassword">Password:</label>
          <br /><input type="password" title="Enter your password" name="txtPassword" /></p>

          <p><input type="submit" name="Submit" value="Login" /></p>

      </form>

      <?php

    } else {

      ?>


  	<div class="p-width-2" id="rightDiv">
  	</div>

  <?php } ?>

    </body>

<!-- js -->
<script type="text/javascript" src="js/back/datas.js"></script>

</html>
