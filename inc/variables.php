<?php

    # PATH
    $datasDir = "../../../datas";
    $trashDir = "../../../trash";
    $countFile = $datasDir . "/count";

    # FIXE
    $membreArr = array(
	"Alexandre Liziard"=>"",
	"Alice Lapalu"=>"",
	"Antoine Gelgon"=>"",
	"Baptiste Tosi"=>"",
	"Cecilia Borettaz"=>"",
	"Claire Williams"=>"",
	"Émile Greis"=>"",
	"Étienne Ozeray"=>"http://etienneozeray.fr",
	"Léonard Mabille"=>"",
	"Maria Delamare"=>"",
	"Morgane Trouillet"=>"",
	"Romain Marula"=>"",
	"Stéphanie Verin "=>"",
    );

    $linkArr = array(
	"Événements"=>"http://evenements.atelier-bek.be",
	"Ressources"=>"http://ressources.atelier-bek.be",
	"Chantiers"=>"http://chantier.atelier-bek.be",
	"Typothèque"=>"http://typotheque.atelier-bek.be",
	"GitLab"=>"https://gitlab.com/groups/atelier-bek",
    );
    

    # DATAS POST 
    $authorArr = array(
	"Bek",
	"Alexandre",
	"Alice",
	"Antoine",
	"Baptiste",
	"Cecilia",
	"Claire",
	"Émile",
	"Étienne",
	"Léonard",
	"Luccas",
	"Matthieu",
	"Maria",
	"Marie",
	"Morgane",
	"Romain",
	"Stéphanie",
    );

    $categoryArr = array(
	"Événements",
	"Ressources",
	"Chantiers",
	"Typothèque",
	"Références",
	"GitLab"
    );

?>
