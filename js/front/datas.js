// --------------------



// # INIT

window.onload = datasGet();

// --------------------

// # DATAS VARIABLES
function datasVariables(){
    console.log("function : datasVariables");

}

// --------------------

// # DATAS EVENTS LISTENERS
function datasEventsListeners() {
    console.log("function : datasEventsListeners");
}

// --------------------

// # DATAS GET
function datasGet(){
  console.log("function : datasGet");

  datasDiv = document.getElementById("datasDiv");

  // ## REQUEST
  var request = $.ajax({
  	url: "php/front/datasGet.php",
  	type: "GET",
  });

  // ## DONE
  request.done(function(content) {


  	datasDiv.innerHTML = content;

  	imageArr = document.getElementsByClassName("image");

  	// imageArr -> hover -> function : seeImage
  	for (i = 0; i < imageArr.length; i ++) {
  	    imageArr[i].addEventListener("click", seeImage);
  	}

  	// datasSort();
          // To keep track of how many images have loaded
    var loaded = 0;

      // Let's retrieve how many images there are
    var numImages = $("img").length;
    $('img').hide();

      // Let's bind a function to the loading of EACH image
    $("img").load(function() {

          // One more image has loaded
        ++loaded;

          // Only if ALL the images have loaded
        if (loaded === numImages) {
          $('.chargement').hide();
          $('img').show();
          imgWidth();

        }
    });

  });
}



// --------------------

function imgWidth(){
  $('img').each(function(){
    var imgW = $(this).get(0).naturalWidth;
    var imgH = $(this).get(0).naturalHeight;
    var width = 200 + Math.floor(Math.random() * 800);
    var height = 200 + Math.floor(Math.random() * 700);
    console.log($(this).parent().parent().attr('id'));
    console.log('imgW=' + imgH);
    console.log('width=' + height);
    if (imgW > imgH && width < imgW){
      $(this).width(width);
    } else if (imgW <= imgH && height < imgH){
      $(this).height(height);
    }
  })
}

// --------------------

function datasSort(){
  console.log("function : datasSort");

  datasDiv = document.getElementById("datasDiv");

  var itemArr = document.getElementsByClassName("item");
  console.log(itemArr);

  var sortArr = [];
  for (var i = 0; i < itemArr.length; i++) {

  	var sendDateTimeStr = itemArr[i].className.split(" ")[0];
  	console.log(sendDateTimeStr);

  	var id = itemArr[i].id;
  	console.log(id);

  	sortArr.push([sendDateTimeStr, itemArr[i]]);
	}

  console.log(sortArr);
  sortArr.sort();
  sortArr.reverse();
  console.log(sortArr);

  // for (var i = 0; i < sortArr.length; i++) {
  // 	datasDiv.appendChild(sortArr[i][1]);
  // }
}

// --------------------

function seeImage(){
  console.log("function : seeImage");
  var naturalW = $(this).get(0).naturalWidth;
  if(naturalW > 800 ){
    $(this).width(800);
    $(this).height('auto');
  } else {
    $(this).width(naturalW);
    $(this).height('auto');
  }
}
