$(document).ready(function(){

  // $("img.lazy").lazyload();

  var grid      = $('#grid');

  var datasDiv  = $('#datasDiv');
  var items     = datasDiv.children('.item');

  var sort      = $('#sort');
  var colonnes  = sort.children('.colonne');
  var bouton    = sort.children('h1');

  var cat       = colonnes.children('li');
  var sousCat   = colonnes.children('ul').children('li');

  var nav       = $('#nav');

  //cacher ressources et références !!! TEMPORAIRE MA GUEULE
  datasDiv.children('.Ressources').hide();
  datasDiv.children('.References').hide();


  // imgWidth();
  imagesThumb(datasDiv);
  showSort(sort, colonnes, bouton)
  sortItems(colonnes, cat, datasDiv);
  sortItems(colonnes, sousCat, datasDiv);
  showGrid(grid);
  showItems(items, datasDiv);
  previewCat(datasDiv, nav)
  hidePrez()
})


function showSort(sort, colonnes, bouton){

  var visible   = false;

  colonnes.hide();

  bouton.click(function(){
    if (visible == false){
      sort.animate({
        "width": "31.57894737%"
      });
      colonnes.slideDown('150');
      visible = true;
    } else {
      colonnes.slideUp('150');
      visible = false;
      sort.animate({
        "width": "5.263157895%"
      });
    }
  })

}

function sortItems(colonnes, cat, datasDiv){


  cat.click(function(){
    if ($(this).attr('class') == 'checked'){
      $(this).removeClass('checked').addClass('unchecked');
      var theCat = $(this).attr('id');
      datasDiv.children('.'+theCat).fadeOut('100');
    } else {
      $(this).addClass('checked').removeClass('unchecked');
      var theCat = $(this).attr('id');
      datasDiv.children('.'+theCat).fadeIn('100');
    }
  })

}

function imagesThumb(datasDiv){
  var itemImages = datasDiv.children('.item').children('.images');
    // var width = ['5.263157895%', '10.52631579%', '15.789473685%']
    var width = ['6.25%', '12.5%', '18.75%']
    itemImages.each(function(){
    var rand = 1 + Math.floor(Math.random() * 3);
    var first = $(this).children('img').first();
    $(this).children('img').hide();
    first.show();
    $(this).parent().width(width[rand-1]);
  })
}

function showGrid(grid){
  grid.hide();
  $(document).mouseup(function(){
    grid.hide();
  }).mousedown(function(){
    grid.show();
  })
}

function showItems(items, datasDiv){
  $('.imagesBig').hide();
  items.click(function(){
    var imagesBig = $(this).children('.imagesBig');
    var infos     = $(this).children('.infos');
    if (imagesBig.is(':visible')){
      datasDiv.children('.item').removeClass('right');
      $('#textPrez').show();
      imagesBig.hide();
      infos.removeClass('infosBig');
      infos.children('p').hide();
    } else {
      datasDiv.children('.item').addClass('right');
      $('#textPrez').hide();
      imagesBig.children('img').each(function(){
      var theSrc = $(this).attr('data-original');
      $(this).attr('src', theSrc);
      })
      infos.addClass('infosBig');
      infos.children('p').show();
      imagesBig.show();
    }
  })
}

function previewCat(datasDiv, nav){

  function show(e){
    var cat = e.attr('class');
    if(cat == 'blog'){
      datasDiv.children('.item').show();
    } else {
      datasDiv.children('.item').hide();
      datasDiv.children('.'+cat).show();
    }
  }

  function hide(){
    datasDiv.children('.item').show();
    datasDiv.children('.Ressources').hide();
    datasDiv.children('.References').hide();
  }

  nav.children('ul').children('li').mouseenter(function(){
    show($(this));
  }).mouseleave(function(){
    hide()
  })

  nav.children('li').mouseenter(function(){
    show($(this));
  }).mouseleave(function(){
    hide();
  })
}

function hidePrez(){
  $('#textPrez').children('span').click(function(){
    $('#textPrez').hide();
    $('#datasDiv').css({'top': '9px'});
  })
}
