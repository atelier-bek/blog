<!DOCTYPE html>
<html>

    <head>

	<!-- inc -->
	<?php include "inc/meta.php"; ?>
	<?php include "inc/title.php"; ?>

	<!-- css -->
	<link rel="stylesheet" type="text/css" href="css/reset.css" />
	<link rel="stylesheet/less" type="text/css" href="css/style.css" />

	<!-- fonts -->
	<link rel="stylesheet" type="text/css" href="fonts/fonts.css" />

	<!-- lib -->
	<script src="lib/less.min.js" type="text/javascript"></script>
	<script src="lib/jquery-2.1.4.min.js" type="text/javascript"></script>
  <script src="js/jquery.lazyload.min.js"></script>

    </head>

    <body>

<?php include 'php/front/nav.php'; ?>
<?php include 'php/front/textPrez.php'; ?>


	<div class="p-width-2" id="datasDiv">

<?php


  $a = array('à', 'À', 'é', 'É', 'è', 'È', 'ê', 'Ê', 'ç', 'Ç', 'ù', 'Ù', '-', "'", ',');
  $b = array('a', 'A', 'e', 'E', 'e', 'E', 'e', 'E', 'c', 'C', 'u', 'U', '', '', ' ');

  $dirs = array();
  $MyDirectory = opendir('../datas') or die('Erreur');
  while($Entry = readdir($MyDirectory)) {
    if(is_dir($Directory.'/'.$Entry)&& $Entry != '.' && $Entry != '..') {
    } elseif ($Entry !='.' && $Entry != '..') {
      // echo $Entry.'<br>';
      array_push($dirs,$Entry);
    }
  }
  closedir($MyDirectory);

  sort($dirs);
  $dirs = array_reverse($dirs);

  $count=count($dirs);

  for ($i = 0; $i < $count; $i++){

    if ($dirs[$i] == 'count') continue;

    // echo $dirs[$i].'<br>';

    $dir0 = '../datas/'.$dirs[$i].'/images_resized';
    $dir = '../datas/'.$dirs[$i].'/images_resized_800';
    $dir2 = '../datas/'.$dirs[$i].'/images_sources';
    $xml = simplexml_load_file('../datas/'.$dirs[$i].'/datas.xml');
    $text = $xml->text;
    $category = $xml->category;
    $author = $xml->author;
    $date = $xml->date;

    $categorySlug = str_replace($a, $b, $category);
    $authorSlug = str_replace($a, $b, $author);
    ?>

    <div id="<?= $dirs[$i] ?>" class="item <?php echo $categorySlug.' '.$authorSlug.' '.$date; ?>">


      <div class="infos">
        <?php
        echo $text;
         echo '<p>posté par '.$author.' dans la catégorie '.$category.'</p>';
        ?>
      </div>
      
      <div class="images">

        <?php
        if (is_dir($dir0)) {
          if ($dh = opendir($dir0)) {

            $j=0;

            $images=array();

            while (($file = readdir($dh)) !== false) {
              $images[]=$file;
            }
            sort($images);



              ?>
              <img class="image resized lazy" src="<?php echo $dir0.'/'.$images[2]; ?>" />
              <?php

              $j++;

            closedir($dh);
          }
          ?>

        </div>

        <?php
      }
      if (is_dir($dir)) {
        if ($dh = opendir($dir)) {

          $j=0;

          $images=array();

          while (($file = readdir($dh)) !== false) {
            $images[]=$file;
          }
          sort($images);

          ?>

          <div class="imagesBig">

            <?php

            foreach($images as $file){

              if( $file != '.' && $file != '..' && preg_match('#\.(jpe?g|gif|png|svg)$#i', $file)) {


              ?>
              <img class="image resized800 lazy" data-original="<?php echo $dir.'/'.$file; ?>" />
              <?php

              $j++;
              }
            }
          closedir($dh);
          ?>

        </div>

        <?php
        }

      } else {
        if ($dh = opendir($dir2)) {

          $j=0;

          $images=array();

          while (($file = readdir($dh)) !== false) {
            $images[]=$file;
          }
          sort($images);


          ?>

          <div class="imagesBig">

            <div class="infos">
              <?php
              echo $text;
               echo '<p>posté par '.$author.' dans la catégorie '.$category.'</p>';
              ?>
            </div>

            <?php

            foreach($images as $file){

              if( $file != '.' && $file != '..' && preg_match('#\.(jpe?g|gif|png|svg)$#i', $file)) {

              ?>
              <img class="image source lazy" data-original="<?php echo $dir2.'/'.$file; ?>" />
              <?php

              $j++;
              }
            }
            closedir($dh);
            ?>

          </div>

          <?php
        }
      }
    ?>
  </div>
  <?php
  }

?>
	</div>


  <!-- 19 x 11 -->
  <div id="grid">
    <?php
      for($row=1; $row <= 11; $row++){
        for($col=1; $col <= 19; $col++){
          echo '<div class="grid col'.$col.' row'.$row.'"><!--⦁--></div>';
        }
      }
    ?>
  </div>

  <?php include('php/front/sort.php'); ?>


    </body>

<!-- js -->
<!-- <script type="text/javascript" src="js/front/datas.js"></script> -->
<script type="text/javascript" src="js/front/main.js"></script>

</html>
